#lang racket/base

(require "aoc.rkt"
         racket/match
         racket/string
         racket/set)

(define (day-7)  
  (define (parse-bag str)
    (match-let ([(pregexp #px"^(\\w+) (\\w+) bags?$" (list _ mod color)) str])
      (string->symbol (format "~a-~a" mod color))))

  (define-app-pattern ~>bag parse-bag)

  (define (parse-bag-count str)
    (match-let ([(pregexp #px"^(\\d+) (.+)$" (list _ count (~>bag bag))) str])
      (cons bag (string->number count))))
   
  (define (parse-rule str)
    (match-let ([(pregexp #px"^(.+) contain (.+)\\.$" (list _ (~>bag superbag) subbags)) str])  
      (cons superbag
            (make-hash (map parse-bag-count
                            (if (string=? subbags "no other bags")
                                '()
                                (string-split subbags ", ")))))))

  (define rules (make-hash (read-input-file "day_7_input.txt"
                                            (map-line parse-rule))))

  (define (find-suitable-outer-bags bags)
    (set-union bags (for/set ([(superbag subbags) rules]
                              #:when (for/or ([bag bags]) (hash-has-key? subbags bag)))
                      superbag)))
  
  (define part-1 (sub1 (set-count ; Sub1 because we don't want to count the gold bag
                        (loop-while-changing (set 'shiny-gold) find-suitable-outer-bags set=?))))

  (define (count-inner-bags bag)
    (+ 1 (for/sum ([(subbag count) (hash-ref rules bag)])
           (* count (count-inner-bags subbag)))))

  (define part-2 (sub1 (count-inner-bags 'shiny-gold))) ; Sub1 to not count the gold bag

  (assert-equal? 300 part-1)
  (assert-equal? 8030 part-2)
  (show-vars part-1 part-2))

(day-7)