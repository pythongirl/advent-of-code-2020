#lang racket/base

(require "aoc.rkt"
         racket/match
         racket/stream)

(define (day-15)
  (struct van-eck-iter (numbers-last-seen current-number current-position)
    #:methods gen:stream
    [(define (stream-empty? stream) #f)
     (define (stream-first stream)
       (van-eck-iter-current-number stream))
     (define (stream-rest this)
       (match-let ([(van-eck-iter numbers-last-seen current-number current-position) this])
         (van-eck-iter (hash-set numbers-last-seen current-number current-position)
                       (match (hash-ref numbers-last-seen current-number #f)
                         [#f 0]
                         [x (- current-position x)])
                       (+ 1 current-position))))])

  ;(define (van-eck-iter-ref iter n)
    
  
  (define input '(11 18 0 20 1 7 16))

  (define (elf-game->van-eck-iter starting-numbers)
    (let ([rev-starting-numbers (reverse starting-numbers)]
          [l (length starting-numbers)])
      (van-eck-iter (make-immutable-hash (for/list ([n (cdr rev-starting-numbers)]
                                                    [i (in-range (- l 2) -1 -1)])
                                           (cons n i)))
                    (car rev-starting-numbers)
                    (- l 1))))

  (define (elf-game-ref starting-numbers n)
    (stream-ref (elf-game->van-eck-iter starting-numbers) (- n (length starting-numbers))))

  (define part-1 (elf-game-ref input 2020))
  ;(define part-2 (elf-game-ref input 30000000))
  
  (show-vars part-1 #;part-2 ))

(day-15)