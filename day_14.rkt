#lang racket/base

(require "aoc.rkt"
         racket/match
         racket/string
         racket/list)

(define (day-14)

  (struct set-mask-instr (mask) #:transparent)
  (struct set-memory-instr (addr val) #:transparent)
  
  (define/match (parse-instruction str)
    [((pregexp #px"mask = ([01X]{36})" (list _ mask)))
     (set-mask-instr mask)]
    [((pregexp #px"mem\\[([0-9]+)\\] = ([0-9]+)" (list _ (~>number addr) (~>number val))))
     (set-memory-instr addr val)])
  
  (define program (read-input-file "day_14_input.txt" (map-line parse-instruction)))

  (struct vm (positive-mask negative-mask memory))
  (define (new-vm) (vm 0 0 (make-immutable-hash '())))

  (define (vm-execute-instr-part-1 my-vm instr)
    (match-let ([(vm positive-mask negative-mask memory) my-vm])
      (match instr
        [(set-mask-instr mask)
         (vm (string->number (string-replace mask "X" "0") 2)
             (string->number (string-replace mask "X" "1") 2)
             memory)]
        [(set-memory-instr addr val)
         (vm positive-mask negative-mask
             (hash-set memory addr (bitwise-and (vm-negative-mask my-vm)
                                                (bitwise-ior (vm-positive-mask my-vm)
                                                             val))))])))

  (define part-1 (for/sum ([(addr val) (vm-memory (for/fold ([my-vm (new-vm)])
                                                            ([instr program])
                                                    (vm-execute-instr-part-1 my-vm instr)))])
                   val))

  (define (hamming-weight b)
    (for/sum ([i (in-range 1 (+ 1 (integer-length b)))] #:when (bitwise-bit-set? b i)) 1))

  (define (hamming-distance a b)
    (hamming-weight (bitwise-xor a b)))
  
  (struct vm2 ([positive-mask #:mutable] [floating-mask #:mutable] memory))
  (define (vm2-execute-instr! my-vm instr)
    (match-let ([(vm2 positive-mask floating-mask memory) my-vm])
      (match instr
        [(set-mask-instr mask)
         (set-vm2-positive-mask! my-vm (string->number (string-replace mask "X" "0") 2))
         (set-vm2-floating-mask! my-vm (string->number (string-replace (string-replace mask "1" "0") "X" "1") 2))]
        [(set-memory-instr addr val)
         (let ([base-addr (bitwise-ior positive-mask
                                       (bitwise-and (bitwise-not floating-mask) addr))])
           (for ([float-bits (in-combinations (for/list ([i 36]
                                                         #:when (bitwise-bit-set? floating-mask i))
                                                (arithmetic-shift 1 i)))])
             (let ([float-addr (apply + float-bits)])
               (hash-set! memory
                          (bitwise-ior base-addr float-addr)
                          val))))])))

  (define part-2 (for/sum ([(addr val) (vm2-memory (let ([my-vm (vm2 0 0 (make-hash '()))])
                                                     (for ([instr program])
                                                       (vm2-execute-instr! my-vm instr))
                                                     my-vm))])
                   val))
  
  (show-vars part-1 part-2))

(day-14)