#lang racket/base

(require "aoc.rkt"
         racket/match
         racket/list
         racket/set
         memoize)

(define/match (parse-hand lines)
  [((list (regexp #rx"Player ([12]):" (list _ (~>number player-number)))
          (~>number cards) ...))
   cards])

(define (score-hand hand)
  (for/sum ([i (in-naturals 1)]
            [card (reverse hand)])
    (* card i)))

(define (combat player-1-hand player-2-hand)
  (do ([player-1-hand player-1-hand (cdr player-1-hand)]
       [player-2-hand player-2-hand (cdr player-2-hand)])
    ((or (empty? player-1-hand) (empty? player-2-hand))
     ; Return the winner's score
     (max (score-hand player-1-hand)
          (score-hand player-2-hand)))
    (let ([player-1-card (car player-1-hand)]
          [player-2-card (car player-2-hand)])
      (if (> player-1-card player-2-card)
          (set! player-1-hand (append player-1-hand (list player-1-card player-2-card)))
          (set! player-2-hand (append player-2-hand (list player-2-card player-1-card)))))))

(define (recursive-combat player-1-hand player-2-hand)
  (define previous-game-states (mutable-set))
  (do ([player-1-hand player-1-hand (cdr player-1-hand)]
       [player-2-hand player-2-hand (cdr player-2-hand)])
    ((or (set-member? previous-game-states (cons player-1-hand player-2-hand)) ; Infinite game rule
         (empty? player-1-hand)
         (empty? player-2-hand))
     (if (set-member? previous-game-states (cons player-1-hand player-2-hand))
         ; Infinite game rule means player 1 wins
         (cons #t (score-hand player-1-hand))
         (let ([player-1-score (score-hand player-1-hand)]
               [player-2-score (score-hand player-2-hand)])
           (cons (> player-1-score player-2-score)
                 (max player-1-score player-2-score)))))
    (set-add! previous-game-states (cons player-1-hand player-2-hand))
    (let* ([player-1-card (car player-1-hand)]
           [player-2-card (car player-2-hand)]
           [player-1-win? (if (and (< player-1-card (length player-1-hand))
                                   (< player-2-card (length player-2-hand)))
                              (match-let ([(cons player-1-win? _) (recursive-combat (take (cdr player-1-hand) player-1-card)
                                                                                    (take (cdr player-2-hand) player-2-card))])
                                player-1-win?)
                              (> player-1-card player-2-card))])
      (if player-1-win?
          (set! player-1-hand (append player-1-hand (list player-1-card player-2-card)))
          (set! player-2-hand (append player-2-hand (list player-2-card player-1-card)))))))
  

(define (day-22)
  (match-define (list player-1-hand player-2-hand) (read-input-file "day_22_input.txt" (map-lines-group parse-hand)))

  (define part-1 (combat player-1-hand player-2-hand))
  (define part-2 (match-let ([(cons _ winner-score) (time (recursive-combat player-1-hand player-2-hand))])
                   winner-score))
  
  (show-vars part-1 part-2))

(day-22)